# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Changing field 'Address.owner_user'
        db.alter_column(u'prop_address', 'owner_user_id',
                        self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.User'], null=True))

    def backwards(self, orm):
        # User chose to not deal with backwards NULL issues for 'Address.owner_user'
        raise RuntimeError("Cannot reverse this migration. 'Address.owner_user' and its values cannot be restored.")

        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Address.owner_user'
        db.alter_column(u'prop_address', 'owner_user_id',
                        self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.User']))

    models = {
        u'prop.address': {
            'Meta': {'object_name': 'Address'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner_user': ('django.db.models.fields.related.ForeignKey', [],
                           {'default': 'None', 'to': u"orm['prop.User']", 'null': 'True'}),
            'provider': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'uid': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '222'})
        },
        u'prop.outertransaction': {
            'Meta': {'object_name': 'OuterTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '18', 'decimal_places': '2'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.User']"})
        },
        u'prop.paymenttask': {
            'Meta': {'object_name': 'PaymentTask'},
            'address_to': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.Address']"}),
            'comment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True'}),
            'grade': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner_user': ('django.db.models.fields.related.ForeignKey', [],
                           {'related_name': "'payment_tasks'", 'to': u"orm['prop.User']"}),
            'user_to': ('django.db.models.fields.related.ForeignKey', [],
                        {'related_name': "'incoming_payment_tasks'", 'null': 'True', 'to': u"orm['prop.User']"})
        },
        u'prop.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '18', 'decimal_places': '2'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'spawned_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.PaymentTask']"}),
            'user_from': ('django.db.models.fields.related.ForeignKey', [],
                          {'related_name': "'out_transactions'", 'to': u"orm['prop.User']"}),
            'user_to': ('django.db.models.fields.related.ForeignKey', [],
                        {'related_name': "'in_transactions'", 'to': u"orm['prop.User']"})
        },
        u'prop.user': {
            'Meta': {'object_name': 'User'},
            'avatar': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'balance': (
            'django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '18', 'decimal_places': '2'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': (
            'django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40', 'db_index': 'True'})
        }
    }

    complete_apps = ['prop']