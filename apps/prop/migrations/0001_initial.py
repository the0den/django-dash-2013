# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'prop_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40, db_index=True)),
            ('balance', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=18, decimal_places=2)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('avatar', self.gf('django.db.models.fields.URLField')(max_length=255, null=True, blank=True)),
            ('nickname', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
        ))
        db.send_create_signal(u'prop', ['User'])

        # Adding model 'Address'
        db.create_table(u'prop_address', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('owner_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.User'])),
            ('provider', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
            ('uid', self.gf('django.db.models.fields.CharField')(default='', max_length=222)),
        ))
        db.send_create_signal(u'prop', ['Address'])

        # Adding model 'PaymentTask'
        db.create_table(u'prop_paymenttask', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('owner_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='payment_tasks', to=orm['prop.User'])),
            ('user_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name='incoming_payment_tasks', null=True, to=orm['prop.User'])),
            ('address_to', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.Address'])),
            ('grade', self.gf('django.db.models.fields.IntegerField')(default=7)),
            ('comment', self.gf('django.db.models.fields.CharField')(default='', max_length=1000)),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True)),
        ))
        db.send_create_signal(u'prop', ['PaymentTask'])

        # Adding model 'Transaction'
        db.create_table(u'prop_transaction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('user_from', self.gf('django.db.models.fields.related.ForeignKey')(related_name='out_transactions', to=orm['prop.User'])),
            ('user_to', self.gf('django.db.models.fields.related.ForeignKey')(related_name='in_transactions', to=orm['prop.User'])),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=18, decimal_places=2)),
            ('spawned_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.PaymentTask'])),
        ))
        db.send_create_signal(u'prop', ['Transaction'])

        # Adding model 'OuterTransaction'
        db.create_table(u'prop_outertransaction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['prop.User'])),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=18, decimal_places=2)),
        ))
        db.send_create_signal(u'prop', ['OuterTransaction'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'prop_user')

        # Deleting model 'Address'
        db.delete_table(u'prop_address')

        # Deleting model 'PaymentTask'
        db.delete_table(u'prop_paymenttask')

        # Deleting model 'Transaction'
        db.delete_table(u'prop_transaction')

        # Deleting model 'OuterTransaction'
        db.delete_table(u'prop_outertransaction')


    models = {
        u'prop.address': {
            'Meta': {'object_name': 'Address'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.User']"}),
            'provider': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'uid': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '222'})
        },
        u'prop.outertransaction': {
            'Meta': {'object_name': 'OuterTransaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '18', 'decimal_places': '2'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.User']"})
        },
        u'prop.paymenttask': {
            'Meta': {'object_name': 'PaymentTask'},
            'address_to': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.Address']"}),
            'comment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True'}),
            'grade': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payment_tasks'", 'to': u"orm['prop.User']"}),
            'user_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'incoming_payment_tasks'", 'null': 'True', 'to': u"orm['prop.User']"})
        },
        u'prop.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '18', 'decimal_places': '2'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'spawned_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['prop.PaymentTask']"}),
            'user_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_transactions'", 'to': u"orm['prop.User']"}),
            'user_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_transactions'", 'to': u"orm['prop.User']"})
        },
        u'prop.user': {
            'Meta': {'object_name': 'User'},
            'avatar': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '18', 'decimal_places': '2'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40', 'db_index': 'True'})
        }
    }

    complete_apps = ['prop']