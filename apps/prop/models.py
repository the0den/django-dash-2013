from decimal import Decimal
from urlparse import urlparse
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, UserManager
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import F, Sum
from django.db.models.signals import post_init
from django.utils import timezone


class NegativeBalanceException(Exception):
    def __init__(self, neg_value):
        self.value = neg_value

    def __str__(self):
        return repr(self.value)


class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)

    username = models.CharField(max_length=40, unique=True, db_index=True)
    balance = models.DecimalField(max_digits=18, decimal_places=2, default=0)
    email = models.EmailField(max_length=254, blank=True)

    # standard stuff
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    avatar = models.URLField(max_length=255, null=True, blank=True)
    nickname = models.CharField(max_length=255, default='')

    daily_charge = models.DecimalField(max_digits=18, decimal_places=2, default=0)

    def set_daily_charge(self, new_daily_charge):
        self.daily_charge = new_daily_charge
        self.save()

    objects = UserManager()

    def full_name(self):
        if self.first_name.strip() or self.last_name.strip():
            return ("%s %s" % (self.first_name.strip(), self.last_name.strip())).strip()
        else:
            return 'User'

    def monthly_charge(self):
        return self.daily_charge * 30

    def daily_receives(self):
        return self.incoming_payment_tasks.annotate(receivements_sum=Sum('amount'))['receivements_sum']

    def add_balance(self, upd_amount):
        TWOPLACES = Decimal(10) ** -2
        upd_amount = Decimal(upd_amount).quantize(TWOPLACES)

        supposed_balance = User.objects.get(pk=self.pk).balance + upd_amount
        if supposed_balance < 0:
            raise NegativeBalanceException(supposed_balance)
        try:
            User.objects.filter(pk=self.pk).update(balance=F('balance') + upd_amount)
        except:#https://code.djangoproject.com/ticket/13666
            pass

    def get_absolute_url(self):
        return reverse('user_info', args=[self.username])


def _add_associate_checks(sender, **kwargs):
    def _associated_with(instance, provider):
        def _associated_with_inner():
            return instance.social_auth.filter(provider=provider).count() > 0

        return _associated_with_inner

    instance = kwargs['instance']
    for social_account_id in settings.SOCIAL_ACCOUNTS.keys():
        setattr(instance, "associated_with_%s" % social_account_id, _associated_with(instance, social_account_id))


post_init.connect(_add_associate_checks, sender=User)


def update_user_details(backend, details, response, user=None, is_new=False,
                        *args, **kwargs):
    if user is None:
        return

    addresses = Address.objects.filter(provider=backend.name, uid=details.get('username'))
    if addresses:
        for adr in addresses:
            adr.owner_user = user
            if adr.balance > 0:
                user.add_balance(adr.balance)
            adr.balance = 0
            adr.save()

            PaymentTask.objects.filter(address_to=adr).update(user_to=user)
    else:
        Address(provider=backend.name, uid=details.get('username'), owner_user=user).save()


class Address(models.Model):
    owner_user = models.ForeignKey(User, null=True, default=None)
    provider = models.CharField(max_length=32, default='')
    uid = models.CharField(max_length=222, default='')
    balance = models.DecimalField(max_digits=18, decimal_places=2, default=0)

    def add_balance(self, upd_amount):
        TWOPLACES = Decimal(10) ** -2
        upd_amount = Decimal(upd_amount).quantize(TWOPLACES)

        supposed_balance = Address.objects.get(pk=self.pk).balance + upd_amount
        if supposed_balance < 0:
            raise NegativeBalanceException(supposed_balance)
        try:
            Address.objects.filter(pk=self.pk).update(balance=F('balance') + upd_amount)
        except:#https://code.djangoproject.com/ticket/13666
            pass

    @classmethod
    def parse_provider_user(cls, data):
        url = urlparse(data)

        domain = url.netloc.split(':')[0]
        if domain.startswith('www.'):
            domain = domain[4:]

        if domain not in settings.SOCIAL_ACCOUNTS_DOMAINS.values():
            provider = False
        else:
            provider = [key for (key, value) in settings.SOCIAL_ACCOUNTS_DOMAINS.items() if value == domain][0]

        account = url.path.split('?')[0].split('/')
        if len(account) == 1 or account[1] == "":
            account = False
        else:
            account = account[1]

        return provider, account


class PaymentTaskManager(models.Manager):
    def active(self, *args, **kwargs):
        kwargs['end_date__isnull'] = True
        return super(PaymentTaskManager, self).get_query_set().filter(*args, **kwargs)


class PaymentTask(models.Model):
    creation_date = models.DateTimeField(default=timezone.now)
    owner_user = models.ForeignKey(User, related_name='payment_tasks')
    user_to = models.ForeignKey(User, null=True, related_name='incoming_payment_tasks')
    address_to = models.ForeignKey(Address)  # should be used only when user_to is null
    grade = models.IntegerField(default=7, choices=[(x, x) for x in range(1, 11)])
    comment = models.CharField(max_length=1000, default='')
    end_date = models.DateTimeField(null=True, default=None) # date when payment was stopped
    # MUST be recomputed each time grade or owner_user.daily_charge is changed
    # introduced to compute received sums more easily
    amount = models.DecimalField(max_digits=18, decimal_places=2, default=0)

    def set_grade(self, new_grade):
        self.grade = new_grade
        self.owner_user.recompute_amounts_given()

    objects = PaymentTaskManager()


class Transaction(models.Model):
    date = models.DateTimeField(default=timezone.now)
    user_from = models.ForeignKey(User, related_name='out_transactions')
    user_to = models.ForeignKey(User, related_name='in_transactions')

    amount = models.DecimalField(max_digits=18, decimal_places=2)
    spawned_by = models.ForeignKey(PaymentTask)


class OuterTransaction(models.Model):
    date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User)
    amount = models.DecimalField(max_digits=18, decimal_places=2) # positive - pay in, neg - pay out

    def to_html_table_row(self):
        return '<tr><td>%s</td><td>%.2f</td></tr>' % (self.date.strftime('%d.%m.%Y %H:%M:%S'), self.amount)