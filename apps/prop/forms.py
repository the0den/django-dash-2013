#coding: utf-8
from crispy_forms.helper import FormHelper
from django import forms
from crispy_forms.bootstrap import InlineRadios
from django.conf import settings
from apps.prop.models import Address


class DailyChargeForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DailyChargeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.error_text_inline = False
        self.helper.form_tag = False
        self.helper.label_class = 'col-lg-1'
        self.helper.field_class = 'col-lg-2'

    daily_charge = forms.DecimalField(max_digits=18, decimal_places=2, min_value=0)


class PayForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(PayForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.error_text_inline = False
        self.helper.form_tag = False
        self.helper.label_class = 'col-lg-1'
        self.helper.field_class = 'col-lg-2'

    amount = forms.DecimalField(max_digits=18, decimal_places=2, min_value=0.01)


class PaymentTaskNewForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(PaymentTaskNewForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.error_text_inline = False
        self.helper.form_tag = False
        self.helper.filter_by_widget(forms.RadioSelect).wrap(InlineRadios)
        self.helper.label_class = 'col-lg-1'
        self.helper.field_class = 'col-lg-5'

    url = forms.URLField(max_length=222, initial='http://')
    grade = forms.ChoiceField(choices=[(x, x) for x in range(1, 11)], initial=7, widget=forms.RadioSelect)
    comment = forms.CharField(max_length=1000, required=False)

    def clean_url(self):
        data = self.cleaned_data['url']
        provider, account = Address.parse_provider_user(data)

        if account is False:
            raise forms.ValidationError(
                'User account is empty'
            )

        if provider is False:
            raise forms.ValidationError(
                'We not support this account type (supported types: %s)' % ', '.join(settings.SOCIAL_ACCOUNTS.keys())
            )

        return data

