from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^profile$', 'apps.prop.views.profile', name='profile'),
    url(r'^pay_in$', 'apps.prop.views.pay_in', name='pay_in'),
    url(r'^pay_out$', 'apps.prop.views.pay_out', name='pay_out'),

    url(r'^ptasks/add/$', 'apps.prop.views.payment_task_add', name='payment_task_add'),
    url(r'^ptasks/(?P<ptask_id>\d+)/edit', 'apps.prop.views.payment_task_edit', name='payment_task_edit'),
    url(r'^ptasks/(?P<ptask_id>\d+)/delete', 'apps.prop.views.payment_task_delete', name='payment_task_delete'),

    url(r'^users/(?P<username>[\w\d\-_]{1,40})/$', 'apps.prop.views.user_info', name='user_info'),
)