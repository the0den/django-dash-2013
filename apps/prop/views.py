import datetime
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from apps.prop.forms import PayForm, PaymentTaskNewForm, DailyChargeForm
from apps.prop.models import NegativeBalanceException, OuterTransaction, PaymentTask, User, Address


@login_required
def profile(request):
    latest_outer_trans = request.user.outertransaction_set.order_by('-date')[:5]
    form = DailyChargeForm(request.POST or {'daily_charge': request.user.daily_charge})
    if form.is_valid() and request.POST:
        request.user.set_daily_charge(form.cleaned_data['daily_charge'])
    return render(request, 'prop/profile.html', {
        'social_accounts': settings.SOCIAL_ACCOUNTS,
        'latest_outer_trans': latest_outer_trans,
        'props': request.user.payment_tasks.active(owner_user=request.user).select_related(),
        'form': form,
    })


@login_required
def pay_in(request):
    form = PayForm(request.POST or None)
    if form.is_valid():
        request.user.add_balance(form.cleaned_data['amount'])
        OuterTransaction(user=request.user, amount=form.cleaned_data['amount']).save()
        return redirect('profile')
    return render(request, 'prop/pay_in.html', {'form': form})


@login_required
def pay_out(request):
    form = PayForm(request.POST or None)
    if form.is_valid():
        try:
            request.user.add_balance(-form.cleaned_data['amount'])
        except NegativeBalanceException as e:
            messages.error(request,
                           'You are trying to drive balance to negative value: %.2f. Please, do not.' % e.value)
        else:
            OuterTransaction(user=request.user, amount=-form.cleaned_data['amount']).save()
            return redirect('profile')
    return render(request, 'prop/pay_out.html', {'form': form})


@login_required
def payment_task_add(request, ptask_id=0):
    form = PaymentTaskNewForm(request.POST or None)
    if form.is_valid():
        provider, account = Address.parse_provider_user(form.cleaned_data['url'])
        address, created = Address.objects.get_or_create(provider=provider, uid=account)
        ptask = PaymentTask(
            creation_date=datetime.datetime.now(),
            owner_user=request.user,
            user_to=address.owner_user,
            address_to=address,
            grade=form.cleaned_data['grade'],
            comment=form.cleaned_data['comment']
        )
        ptask.save()
        messages.info(request, 'Task added!')
        return redirect('profile')

    return render(request, 'prop/payment_task_add.html', {
        'form': form
    })


@login_required
def payment_task_edit(request, ptask_id=0):
    form = PaymentTaskNewForm(request.POST or None)
    if form.is_valid():
        return redirect('profile')

    return render(request, 'prop/payment_task_add.html', {
        'form': form
    })


@login_required
def payment_task_delete(request, ptask_id=0):
    ptask = get_object_or_404(PaymentTask, pk=ptask_id, owner_user=request.user)
    ptask.end_date = datetime.date.today()
    ptask.save()
    return redirect('profile')


def user_info(request, username=''):
    user = get_object_or_404(User, username=username)
    return render(request, 'prop/user_info.html', {'user': user, 'social_accounts': settings.SOCIAL_ACCOUNTS})