from decimal import Decimal
from django.core.management.base import BaseCommand
from apps.prop.models import PaymentTask

TWOPLACES = Decimal(10) ** -2


def decim(val):
    return Decimal(val).quantize(TWOPLACES)


class Command(BaseCommand):
    args = ''
    help = 'Executes payments tasks'

    def handle(self, *args, **options):
        prev_user = 0
        grades_sum = {}
        giving_sum = {}
        tasks = PaymentTask.objects.active()

        for task in tasks:
            if task.owner_user_id not in grades_sum:
                grades_sum[task.owner_user_id] = decim(0)
                if task.owner_user.balance > task.owner_user.daily_charge:
                    giving_sum[task.owner_user_id] = task.owner_user.daily_charge
                else:
                    giving_sum[task.owner_user_id] = task.owner_user.balance
            grades_sum[task.owner_user_id] += decim(task.grade)

        for task in tasks:
            giving_amount = decim(
                decim(giving_sum[task.owner_user_id]) / grades_sum[task.owner_user_id] * decim(task.grade))

            if task.user_to is not None:
                task.user_to.add_balance(giving_amount)
            else:
                task.address_to.add_balance(giving_amount)

            task.owner_user.add_balance(-giving_amount)

        self.stdout.write('Successfully executed')