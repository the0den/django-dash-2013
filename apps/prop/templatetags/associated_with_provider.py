from django import template
from django.conf import settings
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def associated_with_provider(context, user, provider):
    if getattr(user, 'associated_with_%s' % provider)():
        try:
            uid = user.address_set.filter(provider=provider)[0].uid
        except:
            uid = ''
            #todo one minute to end dd
        return 'Connected (<a href="http://%s/%s">%s</a>)' % (
            settings.SOCIAL_ACCOUNTS_DOMAINS[provider],
            uid,
            uid
        )
    else:
        if context['request'].user == user:
            return '<a href="%s?next=%s">Connect</a>' % (
                reverse('socialauth_begin', args=[provider]), context['request'].path)
        else:
            return 'Not connected'
