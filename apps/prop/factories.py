import datetime
import factory
from apps.prop.models import User, PaymentTask, Address


class UserFactory(factory.DjangoModelFactory):
    FACTORY_FOR = User

    first_name = factory.Sequence(lambda n: 'Ivan%s' % n)
    last_name = factory.Sequence(lambda n: 'Petrov%s' % n)

    username = factory.Sequence(lambda n: 'user%s' % n)
    email = 'demo@mail.com'
    password = '1234567'

    @classmethod
    def _prepare(cls, create, **kwargs):
        password = kwargs.pop('password', None)
        user = super(UserFactory, cls)._prepare(create, **kwargs)
        if password:
            user.set_password(password)
            if create:
                user.save()
            user.password_safe = password
        return user


class AddressFactory(factory.DjangoModelFactory):
    FACTORY_FOR = Address

    owner_user = factory.SubFactory(UserFactory)
    provider = 'twitter'
    uid = '123'


class PaymentTaskFactory(factory.DjangoModelFactory):
    FACTORY_FOR = PaymentTask

    creation_date = datetime.datetime.now()
    owner_user = factory.SubFactory(UserFactory)
    user_to = factory.SubFactory(UserFactory)
    address_to = factory.SubFactory(AddressFactory)