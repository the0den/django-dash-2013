from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import TestCase
from social_auth.db.django_models import UserSocialAuth
from apps.core.tests import Dev8WebTest
from apps.prop.factories import UserFactory, PaymentTaskFactory
from apps.prop.models import User, PaymentTask, Address


class UserTest(TestCase):
    def setUp(self):
        settings.SOCIAL_ACCOUNTS = {
            'facebook': 'Facebook',
            'twitter': 'Twitter'
        }

    def test_fullname(self):
        user = UserFactory.build(first_name='Ivan', last_name='Petrov')
        assert user.full_name() == "Ivan Petrov"

        user.first_name = 'Ivan '
        assert user.full_name() == "Ivan Petrov"

        user.first_name = ' '
        assert user.full_name() == "Petrov"

        user.last_name = ''
        assert user.full_name() == "User"

        user.first_name = ''
        assert user.full_name() == "User"

    def test_associates_check(self):
        user = UserFactory()
        for provider in settings.SOCIAL_ACCOUNTS.keys():
            assert hasattr(user, 'associated_with_%s' % provider)
            assert callable(getattr(user, 'associated_with_%s' % provider))

        assert not user.associated_with_twitter()
        assert not user.associated_with_facebook()
        UserSocialAuth(user=user, provider='twitter', uid=1).save()
        assert user.associated_with_twitter()
        assert not user.associated_with_facebook()

    def test_add_balance(self):
        user = UserFactory()
        user2 = User.objects.get(pk=user.pk)

        user2.add_balance(2)
        user_from_db = User.objects.get(pk=user.pk)
        assert user_from_db.balance == 2

        user.add_balance(2)
        user_from_db = User.objects.get(pk=user.pk)
        assert user_from_db.balance == 4


class TestViews(Dev8WebTest):
    def setUp(self):
        settings.SOCIAL_ACCOUNTS = {
            'facebook': 'Facebook',
            'twitter': 'Twitter'
        }
        self.user = UserFactory()
        self.user2 = UserFactory()

    def test_profile(self):
        resp = self._get('profile')
        self.assertRedirects(resp, "%s?next=%s" % (reverse('login'), reverse('profile')))

        resp = self._get('profile', self.user)
        assert 'Twitter: <a href' in resp

        UserSocialAuth(user=self.user, provider='twitter', uid=1).save()
        Address(provider='twitter', uid='qqq', owner_user=self.user).save() #todo not executed upfate_user_detail
        resp = self._get('profile', self.user)
        assert 'Twitter: Connected' in resp

    def test_pay_in(self):
        self.assertRedirects(self._get('pay_in'), "%s?next=%s" % (reverse('login'), reverse('pay_in')))
        assert self._get('pay_in', self.user).status_code == 200

    def test_pay_out(self):
        self.assertRedirects(self._get('pay_out'), "%s?next=%s" % (reverse('login'), reverse('pay_out')))
        assert self._get('pay_out', self.user).status_code == 200

    def test_paymenttask_add(self):
        self.assertRedirects(self._get('payment_task_add'),
                             "%s?next=%s" % (reverse('login'), reverse('payment_task_add')))

        resp = self._get('payment_task_add', self.user)
        assert resp.status_code == 200
        assert 'form_edit_task' in resp.forms
        form = resp.forms['form_edit_task']
        form['url'] = 'http://barmitter.com/quantum13'
        form['grade'] = 5
        form['comment'] = '123'
        res_page = form.submit()
        assert u'We not support' in res_page

        resp = self._get('payment_task_add', self.user)
        assert resp.status_code == 200
        assert 'form_edit_task' in resp.forms
        form = resp.forms['form_edit_task']
        form['url'] = 'http://twitter.com/'
        form['grade'] = 5
        form['comment'] = '123'
        res_page = form.submit()
        assert u'User account is empty' in res_page

        resp = self._get('payment_task_add', self.user)
        assert resp.status_code == 200
        tasks_count = PaymentTask.objects.all().count()
        assert 'form_edit_task' in resp.forms
        form = resp.forms['form_edit_task']
        form['url'] = 'http://twitter.com/quantum13'
        form['grade'] = 5
        form['comment'] = '123'
        res_page = form.submit().follow()
        assert u'Task added!' in res_page

        self.assertEqual(PaymentTask.objects.all().count(), tasks_count + 1)

        #todo test user_to with no user and existing user


    def test_paymenttask_edit(self):
        self.assertRedirects(self._get('payment_task_edit', None, 1),
                             "%s?next=%s" % (reverse('login'), reverse('payment_task_edit', args=[1])))

    def test_paymenttask_delete(self):
        self.assertRedirects(self._get('payment_task_delete', None, 1),
                             "%s?next=%s" % (reverse('login'), reverse('payment_task_delete', args=[1])))

        assert self._get('payment_task_delete', self.user, 999).status_code == 404
        task = PaymentTaskFactory(owner_user=self.user, user_to=None)
        assert PaymentTask.objects.all().count() == 1

        assert self._get('payment_task_delete', self.user2, task.pk).status_code == 404
        self.assertRedirects(self._get('payment_task_delete', self.user, task.pk), reverse('profile'))
        assert PaymentTask.objects.all().count() == 1
        assert PaymentTask.objects.active().count() == 0
