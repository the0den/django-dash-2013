from django.conf import settings
from django.db.models import Avg
from django.shortcuts import render, redirect
from apps.prop.models import User


def home(request):
    return render(request, 'core/home.html', {
        'top_givers': User.objects.order_by('-daily_charge')[:5],
        'top_receivers': (
            User.objects
                .annotate(avg_rcv_grade=Avg('incoming_payment_tasks__grade'))
                .filter(avg_rcv_grade__isnull=False)
                .order_by('-avg_rcv_grade')[:5]
        ), # TODO: very complex query; don't eat method - wants field
        'fresh_users': User.objects.order_by('-date_joined')[:5],
    })


def login(request):
    if request.user.is_authenticated():
        return redirect('profile')

    params = {
        'social_accounts': settings.SOCIAL_ACCOUNTS
    }
    if 'next' in request.GET:
        params['next'] = request.GET['next']

    return render(request, 'core/login.html', params)