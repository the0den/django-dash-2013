#coding: utf8
from django.core.urlresolvers import reverse
from django_webtest import WebTest
from apps.prop.factories import UserFactory


class Dev8WebTest(WebTest):
    def _get(self, name, user=None, *args, **kwargs):
        return self.app.get(reverse(name, args=args, kwargs=kwargs), user=user, status='*')


class TestViews(Dev8WebTest):
    def setUp(self):
        self.user = UserFactory()

    def test_home(self):
        resp = self._get('home')
        assert resp.status_code == 200

    def test_login(self):
        resp = self._get('login')
        self.assertTemplateUsed(resp, 'core/login.html')

        resp = self._get('login', self.user)
        self.assertRedirects(resp, reverse('profile'))


    def test_logout(self):
        resp = self._get('profile', self.user)
        assert self.user.full_name() in resp

        assert u'Logout' in resp
        page = resp.click(u'Logout').follow()
        assert u'Logout' not in page