from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', 'apps.core.views.home', name='home'),

    url(r'^login/$', 'apps.core.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),

)