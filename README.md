Solid prop
==========

Solid prop is a proof of concept for sustainable crowdfunding. Demo version is available at http://solidprop.dev8.ru/. **None of real money are envolved.**

The source code is distributed under [GPLv2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html) license.


Installation
------------

1. Clone repo
2. Run `pip install -r requirements.txt`
3. Copy `solidprop/settings_local.py.example` to `solidprop/settings_local.py` and set db settings
4. Create applications on third part services for social auth (in `solidprop/settings_local.py.example` exist keys for `localhost:8000` host)
4.1. Create application in Github account settings and set GITHUB_APP_ID and GITHUB_API_SECRET in `solidprop/settings_local.py`
4.2. Create application in Twitter account settings and set TWITTER_CONSUMER_KEY and TWITTER_CONSUMER_SECRET in `solidprop/settings_local.py`
4.3. Create application in Facebook account settings and set FACEBOOK_APP_ID and FACEBOOK_API_SECRET in `solidprop/settings_local.py`
4. Run `python manage.py syncdb`
5. Run `python manage.py migrate`
6. Run `python manage.py runserver`
7. Open in your browser http://localhost:8000/
8. Add to cronjobs (daily) `python path/to/project/manage.py execute_tasks`


Short usage instruction
-----------------------

1. Login with your github account (twitter and facebook are planned)
2. Set daily amount of your props funding
3. Give grades to some other users of Solid prop or even for someone outside of SP! (github, twitter and facebook are supported at the moment)
4. That's it: you have provided persons valueable for you with solid props! Once a day your daily charge will be distributed among the persons chosen proportionally to grades given.
5. Enjoy the win of The Great Justice!


Comprehensive description
-------------------------

We suppose that the whole idea is pretty clear since many *alike* services exist already. So here are the list of main design *distinctive* points:

 * daily basis
 * ability to prop the *out of service user* (yes, to be really propped she would have to register, but engaging is available before the registration; no notifications of user being propped in current implementation, unfortunately)
 * possibility of single prop (not implemented during dash at all)
 * indirect setting of the prop amount (User don't say: "Joe, you'll have 5 cents per day and you, Jill, - 7 cents". Instead user sets her total daily charges which would be distributed according to grades given)

P.S.: no gittip was harmed during development of this concept.

Always opened to feedback and suggestions,  
Denis Untevskiy, Vladimir Hramov