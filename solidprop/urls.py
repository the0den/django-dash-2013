from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    '',
    url(r'', include('apps.core.urls')),
    url(r'', include('apps.prop.urls')),


    url(r'', include('social_auth.urls')),

)